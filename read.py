import openpyxl
def read_data():
    mass = []

    sheet = openpyxl.load_workbook("data.xlsx").active
    row = sheet.max_row
    for i in range(1, row + 1):
        cell = sheet.cell(row=i, column=1)
        if cell.value is not None:
            mass.append(cell.value)
        else:
            break
    return mass
