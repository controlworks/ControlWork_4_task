import read
import show
from scipy.signal import medfilt


def main():
    exponential()
    median()


def exponential():
    prev = 0
    mass_exponent = []
    for step, mass_value in enumerate(read.read_data()):
        if step == 0:
            prev = mass_value
        period = step + 1
        constant = 2 / (period + 1)
        total = mass_value * constant + prev * (1 - constant)
        prev = total
        mass_exponent.append(prev)

    show.chart(read.read_data(), mass_exponent)


def median():
    show.chart(read.read_data(), medfilt(medfilt(read.read_data())))


main()
